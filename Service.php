<?php

	Class Service{

		public function exposeApi()
		{
			
			$target_file = basename("services.csv");
			// str_getcsv() is php imbeded function to convert csv string to array
		   	$data = array_map('str_getcsv', file($target_file));
		   	array_walk($data, function(&$header) use ($data) {
		   	//to save header as key we used array combine function
		    $header = array_combine($data[0], $header);
		   	});
		   	array_shift($data);
		   	//returning data array
		  	echo json_encode($data);
		}
	}
	$service = new Service;
	$service->exposeApi();
	
	
		
?>