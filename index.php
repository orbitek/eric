<?php
include('Header.php');

// Using session to display message on save new record
session_start();
if(isset($_SESSION['message']) && !empty($_SESSION['message']))
{
	$msg = $_SESSION['message'];
	echo "<script type='text/javascript'>alert('$msg');</script>";
	session_destroy();
}
?>
	<!-- Container Start -->
    <div class="container mt-5">
        <h4 class="T" > Add New Record</h4>

        <!-- from div -->
        <div class="d-print-none form-group ">
           <form action="SaveRecord.php" method="post">
            <input type="text" name="Ref" id="Ref">
            <input type="text" name="Centre" id="Centre">
            <input type="text" name="Service" id="Service">
            <input type="text" name="Country" id="Country">
 
            <input type="submit" value="Add" class="btn btn-primary" >
           </form>
        </div>
        <!-- !form end -->
        <div>
            <table class="wid table table-bordered "name="mytable" id="mytable">
                <thead class="bg-primary text-white">
                <tr>
                    <th scope="col">Ref</th>
                    <th scope="col">Center</th>
                    <th scope="col">Service</th>
                    <th scope="col">Country</th>
                </tr>
                </thead>
                <!-- Country Table -->
                <tbody id="country_table">

                </tbody>
            </table>
        </div>
        
    </div>
    <!-- !Container End -->
</body>

<script type="text/javascript">
	var Row = "";
    var url = "Service.php";
    // Ajax request
    $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
           contentType: "application/json; charset=UTF-8",
            success: function (data) {
				for (var i = 0; i<data.length ; i++)
	            {
	                var ref = data[i].Ref;
	                var centre = data[i].Centre;
	                var service = data[i].Service;
	                var country = data[i].Country;
	                // creating data row to append in Table 
	                Row += "<tr id='"+ref+"' >";
		                Row +="<td>"+ ref+ "</td>";
		                Row +="<td>"+ centre+ "</td>";
		                Row +="<td>"+ service + "</td>";
		                Row +="<td>"+ country + "</td>";
	                Row +="</tr>";

	            }
	            // appending all rows in innerhtml of country_table
	            document.getElementById("country_table").innerHTML = Row;
	            },
            
        });

    
</script>