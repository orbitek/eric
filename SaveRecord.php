<?php

	// receiving data by Post
	$data = $_POST;
	// fopen is a builtin funtion to open file
	$fp = fopen('services.csv', 'a');
	// builtin funtion
	fputcsv($fp, $data);
	//  builtin function
	fclose($fp);
	// Session to show message on index page
	session_start();
	$_SESSION['message'] = "New Record added";

	//getting back to index page
	header('Location: index.php');
	exit;